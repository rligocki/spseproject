//- načtení údajů ze souboru OK
//- uložení údajů do souboru OK
//- přidání záznamů do databáze OK
//- výpis všech záznamů OK
//- vyhledání záznamu podle zvolené položky (možnost výběru) OK
//- řazení záznamů
//- odstranění určeného záznamu OK
//- oprava údajů OK
//- určení minima, maxima případně součtu, nebo průměru – podle typu údajů v databázi OK

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define soubor "soubor.dat"
#define N 20
#define M 30

typedef struct{
    char jmeno[N];
    char prijmeni[N];
    int vek;
    int hodnoceni;
}Tdevcata;

int menu()
{
    int volba;
    
    
    do{
        printf("Vítejte v mém království\n");
        printf("1) Vypis\n");
        printf("2) Pridani\n");
        printf("3) Hledani\n");
        printf("4) Vypis s razenim\n");
        printf("5) Informace od databazi\n");
        printf("6) Ukoncit program\n\n");
    
        printf("Zadejte vasi volbu: ");
        scanf("%i", &volba);
        getchar();
        printf("\n");
        
    }while(volba > 5 || volba < 1);
    return volba;
}

int zapis(Tdevcata devcata[], int pocet)
{
    FILE *f;
    int i;
    if((f=fopen(soubor,"wb"))==NULL)
    {
        printf("\nSoubor se nepovedlo otevrit\n");
        return -1;
    }
    for(i=0; i<pocet; i++)
    {
        fwrite(&devcata[i], sizeof(Tdevcata), 1, f);
    }
    if (fclose(f)==EOF)
    {
        printf("Soubor se nepodarilo zavrit");
        return -1;
    }
    return 0;
}

int cteni(Tdevcata devcata[])  //    cteni ze souboru
{
    FILE *f;
    Tdevcata x;
    int i = 0, pocet = 0;
    if((f = fopen(soubor,"rb")) == NULL)
    {
        printf("\nSoubor se nepovedlo otevrit\n");
        return -1;
    }
    
    while( fread(&x, sizeof(Tdevcata), 1, f) == 1)
    {
        devcata[i]=x;
        i++;
        pocet++;
    }
    
    fclose(f);
    return pocet;
}

void vypis(Tdevcata devcata[], int pocet)
{
    int i;
    printf("ID |  Jmeno  |  Prijmeni |Vek| Hodnoceni\n");
    for(i = 0 ; i < pocet; i++)
    {
        printf("%2i %10s %10s %4i %4i/10\n", i, devcata[i].jmeno, devcata[i].prijmeni, devcata[i].vek, devcata[i].hodnoceni);
    }
    
    printf("\n\n");
}

int pridat(Tdevcata devcata[], int pocet)
{
    printf("Zadejte jmeno slecny: ");
    gets(devcata[pocet].jmeno);
    
    printf("Zadejte prijmeni slecny: ");
    gets(devcata[pocet].prijmeni);
    
    printf("Zadejte vek slecny: ");
    scanf("%d",&devcata[pocet].vek);
    getchar();
    
    printf("Zadejte hodnoceni slecny: ");
    scanf("%d",&devcata[pocet].hodnoceni);
    getchar();
    pocet++;
    zapis(devcata, pocet);
    return pocet;
}

int smazat(Tdevcata devcata[], int pocet, int nalezeno)
{
    int i;
    for(i = nalezeno; i< pocet-1; i++)
    {
        devcata[i] = devcata[i+1];
    }
    pocet--;
    zapis(devcata, pocet);
    return pocet;
}

void upravit(Tdevcata devcata[], int pocet, int nalezeno)
{
    printf("Uprava %s %s, Vek: %d, Hodnoceni: %d\n", devcata[nalezeno].jmeno, devcata[nalezeno].prijmeni, devcata[nalezeno].vek, devcata[nalezeno].hodnoceni);
    printf("");
    printf("\nZadejte nove jmeno: ");
    gets(devcata[nalezeno].jmeno);
    printf("\nZadejte nove prijmeni: ");
    gets(devcata[nalezeno].prijmeni);
    printf("\nZadejte novy vek: ");
    scanf("%d", &devcata[nalezeno].vek);
    getchar();
    printf("\nZadejte nove hodnoceni: ");
    scanf("%d", &devcata[nalezeno].hodnoceni);
    getchar();
    
    zapis(devcata, pocet);
}

int hledani(Tdevcata devcata[], int pocet)
{
    char hledat[20];
    int i,volba;
    int nalezeno = 0;
    printf("Hledat: ");
    gets(hledat);
    
    for(i = 0; i < pocet; i++)
    {
        if(strcmp(hledat, devcata[i].jmeno) == 0 || strcmp(hledat, devcata[i].prijmeni) == 0)
        {
            nalezeno = i;
            break;
        }
    }
    
    if(nalezeno != 0)
    {
        printf("Podarilo se najit %s %s\n", devcata[nalezeno].jmeno, devcata[nalezeno].prijmeni);
        printf("Co chcete s danym zaznamem provest ?\n");
        printf("1) Vymazat\n");
        printf("2) Upravit\n");
        
        do{
            printf("Zadejte vasi volbu: ");
            scanf("%i", &volba);
            getchar();
        }while(volba > 2 || volba < 1);
        
        switch(volba)
        {
            case 1: pocet = smazat(devcata, pocet, nalezeno);
                break;
            case 2: upravit(devcata, pocet, nalezeno);
                break;
        }
    }
    else{
        printf("Nic se nepovedlo najit");
    }
    return pocet;
    
}

void razeniJmeno(Tdevcata devcata[], int pocet)
{
    int i, j;
    Tdevcata pom;
    for(i = 0; i < pocet; i++)
    {
        for(j = 0; j < pocet-1; j++)
        {
            if(0 < strcmp(devcata[j].jmeno, devcata[j+1].jmeno))
            {
                pom = devcata[j];
                devcata[j] = devcata[j+1];
                devcata[j+1] = pom;
            }
        }
    }
}

void razeniPrijmeni(Tdevcata devcata[], int pocet)
{
    int i, j;
    Tdevcata pom;
    for(i = 0; i < pocet; i++)
    {
        for(j = 0; j < pocet-1; j++)
        {
            if(0 < strcmp(devcata[j].prijmeni, devcata[j+1].prijmeni))
            {
                pom = devcata[j];
                devcata[j] = devcata[j+1];
                devcata[j+1] = pom;
            }
        }
    }
}

void razeniVek(Tdevcata devcata[], int pocet)
{
    int i, j;
    Tdevcata pom;
    for(i = 0; i < pocet; i++)
    {
        for(j = 0; j < pocet-1; j++)
        {
            if(devcata[j].vek > devcata[j+1].vek)
            {
                pom = devcata[j];
                devcata[j] = devcata[j+1];
                devcata[j+1] = pom;
            }
        }
    }
}

void razeniHodnoceni(Tdevcata devcata[], int pocet)
{
    int i, j;
    Tdevcata pom;
    for(i = 0; i < pocet; i++)
    {
        for(j = 0; j < pocet-1; j++)
        {
            if(devcata[j].hodnoceni < devcata[j+1].hodnoceni)
            {
                pom = devcata[j];
                devcata[j] = devcata[j+1];
                devcata[j+1] = pom;
            }
        }
    }
}

void specialVypis(Tdevcata devcata[], int pocet)
{
    int volba;
    printf("Zadejte kriterium razeni\n");
    printf("1) Jmeno\n");
    printf("2) Prijmeni\n");
    printf("3) Vek\n");
    printf("4) Hodnoceni\n");
    
    do{
        printf("Zadejte vasi volbu: ");
        scanf("%d", &volba);
        getchar();
    }while(volba > 4 || volba < 1);
    
    switch(volba)
    {
        case 1: razeniJmeno(devcata, pocet);
            break;
        case 2: razeniPrijmeni(devcata, pocet);
            break;
        case 3: razeniVek(devcata, pocet);
            break;
        case 4: razeniHodnoceni(devcata, pocet);
            break;
    }
    
    vypis(devcata, pocet);
}

void info(Tdevcata devcata[], int pocet)
{
    printf("Informace o databazi\n");
    printf("Pocet zaznamu v databazi: %d\n", pocet);
    razeniHodnoceni(devcata, pocet);
    printf("Nejlepe hodnocena slecna: %s %s, Vek: %d,Hodnoceni: %d\n", devcata[0].jmeno, devcata[0].prijmeni, devcata[0].vek, devcata[0].hodnoceni);
    printf("Nejhur hodnocena slecna: %s %s, Vek: %d,Hodnoceni: %d\n", devcata[pocet-1].jmeno, devcata[pocet-1].prijmeni, devcata[pocet-1].vek, devcata[pocet-1].hodnoceni);
    razeniVek(devcata, pocet);
    printf("Nejmladsi slecna: %s %s, Vek: %d,Hodnoceni: %d\n", devcata[0].jmeno, devcata[0].prijmeni, devcata[0].vek, devcata[0].hodnoceni);
    printf("Nejstarsi slecna: %s %s, Vek: %d,Hodnoceni: %d\n\n", devcata[pocet-1].jmeno, devcata[pocet-1].prijmeni, devcata[pocet-1].vek, devcata[pocet-1].hodnoceni);
}

int main(int argc, const char * argv[]) {
    int volba, pocet;
    Tdevcata devcata[M];
    
    do{
        volba = menu();
        pocet = cteni(devcata);
        
        switch(volba)
        {
            case 1: vypis(devcata, pocet);
                break;
            case 2: pridat(devcata, pocet);
                break;
            case 3: pocet = hledani(devcata, pocet);
                break;
            case 4: specialVypis(devcata, pocet);
                break;
            case 5: info(devcata, pocet);
                break;
        }
        
    }while(volba != 6);
    
    return 0;
}
